<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight flex items-center justify-between">
            {{ __('Products') }}
            @hasrole('admin')
            <a href="{{ route('product.create') }}" class="text-xs bg-gray-800 text-white rounded px-2 py-1">New</a>
            @endhasrole
        </h2>
    </x-slot>

    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white overflow-hidden shadow-sm sm:rounded-lg">
                <div class="p-6 text-gray-900">
                <table class="mb-4">
                    @foreach($products as $product)
                        <tr class="border-b border-gray-200 text-sm">
                            <td class="px-6 py-4">{{ $product->name }}</td>
                            <td class="px-6 py-4">{{ $product->price }} $</td>
                            @hasrole('admin')
                            <td class="px-6 py-4">
                                <a href="{{ route('product.edit', $product) }}" class="text-indigo-600">Edit</a>
                            </td>
                            @endhasrole
                            <td class="px-6 py-4">
                            @hasrole('admin')
                            <form action="{{ route('product.destroy', $product) }}" method="POST">
								    @csrf 
								    @method('DELETE')
								<input 
								    type="submit" 
								    value="Delete" 
								    class=" bg-gray-800 text-white rounded px-4 py-2" 
								    onclick="return confirm('Delete product?')"
								>
							</form>
                            @else
                            <form action="{{ route('purchase.store') }}" method="POST">
								@csrf
                                <input type="hidden" name="product_id" value="{{ $product->id }}">
                                <input type="hidden" name="price_today" value="{{ $product->price }}">
                                <input type="hidden" name="tax_today" value="{{ $product->tax }}">
								<input 
								    type="submit" 
								    value="Buy" 
								    class="bg-gray-800 text-white rounded px-4 py-2" 
								    onclick="return confirm('You sure buy?')"
								>
							</form>
                            @endhasrole
                            </td>
                        </tr>
                    @endforeach
                    </table>
                    {{ $products->links() }}
                </div>
            </div>
        </div>
    </div>
</x-app-layout>
