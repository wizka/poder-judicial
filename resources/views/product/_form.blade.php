@csrf
<label class="uppercase text-gray-700 text-xs">Name</label>
<span class="text-xs text-red-600">@error('name') {{ $message }} @enderror</span>

<input type="text" name="name" class="rounded border-gray-200 w-full mb-4" value="{{ old('name', $product->name) }}">

<label class="uppercase text-gray-700 text-xs">Price</label>
<span class="text-xs text-red-600">@error('price') {{ $message }} @enderror</span>

<input type="text" name="price" class="rounded border-gray-200 w-full mb-4" value="{{ old('price', $product->price) }}">

<label class="uppercase text-gray-700 text-xs">Tax</label>
<span class="text-xs text-red-600">@error('tax') {{ $message }} @enderror</span>

<input type="text" name="tax" class="rounded border-gray-200 w-full mb-4" value="{{ old('tax', $product->tax) }}">

<div class="flex justify-between items-center">
    <a href="{{ route('product.index') }}" class="text-indigo-600">Back</a>

    <input type="submit" value="Save" class="bg-gray-800 text-white rounded px-4 py-2">
</div>