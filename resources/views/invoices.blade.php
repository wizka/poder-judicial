<x-guest-layout>
    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white overflow-hidden shadow-sm sm:rounded-lg">
                <div class="p-6 text-gray-900">
                    <h3>Buy products in three steps.</h3>
                    <ul>
                        <li>
                            1. Navigate to <a class="link" href="{{route('register')}}">Register</a>  or 
                            <a class="link" href="{{route('login')}}">login</a> if you already registered.
                        </li>
                        <li>
                            2. Click "Buy" tab.
                        </li>
                        <li>
                            3. Choose product clicking "buy" button.
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</x-guest-layout>
