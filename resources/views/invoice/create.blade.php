<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Invoice preview') }}
        </h2>
    </x-slot>

    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white overflow-hidden shadow-sm sm:rounded-lg">
                <div class="p-6 text-gray-900">
                    <h1>
                    Client invoice: {{ $user_id }}
                    </h1>
                    <span class="text-xs text-red-600">@error('user_id') {{ $message }} @enderror</span>
                </div>
                <div class="p-6 text-gray-900">
                    <table class="mb-4">
                    @foreach($purchases as $purchase)
                        <tr class="border-b border-gray-200 text-sm">
                            <td class="px-6 py-4">{{ $purchase->product->name }}</td>
                            <td class="px-6 py-4">{{ $purchase->price_today }}$ + ({{ $purchase->tax_today }}$)</td>
                        </tr>
                    @endforeach
                    </table>
                    <table>
                        <tr>
                        <td class="px-6 py-4">Products: {{ $total_price }}</td>
                        <td class="px-6 py-4">Taxes: {{ $total_tax }}</td>
                        <td class="px-6 py-4">Total: <b>{{ $total }}</b></td>
                        @can('check in')
                        <td class="px-6 py-4">
                        <form action="{{ route('invoice.store') }}" method="POST">
							@csrf
                            <input type="hidden" name="user_id" value="{{ $user_id }}">
							<input 
								type="submit" 
								value="Check-in" 
								class="bg-gray-800 text-white rounded px-4 py-2" 
								onclick="return confirm('Check-in?')"
							>
						</form>
                        </td>
                        @endcan
                        </tr>
                    </table>
                </div>
            </div>
        </div>
    </div>
</x-app-layout>
