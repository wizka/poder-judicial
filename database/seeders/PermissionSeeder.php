<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

use App\Models\User;

use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;
use Spatie\Permission\PermissionRegistrar;

class PermissionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Reset cached roles and permissions
        app()[PermissionRegistrar::class]->forgetCachedPermissions();

        // create permissions
        Permission::create(['name' => 'buy']);
        Permission::create(['name' => 'check in']);
        Permission::create(['name' => 'view all invoices']);

        // create roles and assign existing permissions
        $role1 = Role::create(['name' => 'admin']);
        $role1->givePermissionTo('check in');
        $role1->givePermissionTo('view all invoices');

        $role2 = Role::create(['name' => 'client']);
        $role2->givePermissionTo('buy');
        // create a demo user
        $admin = User::factory()->create([
            'name' => 'Admin App',
            'email' => 'admin@invoiceapp.io',
            // factory default password is 'password'
        ]);
        $admin->assignRole($role1);
        // create a demo user
        $client = User::factory()->create([
            'name' => 'Client App',
            'email' => 'client@invoiceapp.io',
            // factory default password is 'password'
        ]);
        $client->assignRole($role2);
    }
}
