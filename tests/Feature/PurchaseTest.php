<?php

namespace Tests\Feature;

use App\Models\Product;
use App\Models\User;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class PurchaseTest extends TestCase
{
    private function get_101_products()
    {
        return Product::factory(101)->create();
    }

    private function create_user()
    {
        return User::factory()->create();
    }
    /**
     * Auth user can buy Product.
     *
     * @return void
     */
    public function test_auth_user_can_buy(): void
    {
        $user = $this->create_user();
        $products = $this->get_101_products(); ///Create products
        $selected_product = $products->first();
        $response = $this
            ->actingAs($user)
            ->post('/purchase', [
                'product_id' => $selected_product->id,
                'price_today' => $selected_product->price,
                'tax_today' => $selected_product->tax,
            ]);

        $response->assertRedirect('/dashboard');
    }
}
