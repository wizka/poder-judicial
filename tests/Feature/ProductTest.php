<?php

namespace Tests\Feature;

use App\Models\User;
use App\Models\Product;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class ProductTest extends TestCase
{
    private function get_101_products()
    {
        return Product::factory(101)->create();
    }

    private function create_user()
    {
        return User::factory()->create();
    }

    private function create_admin()
    {
        return User::factory()->create();
    }
    /**
     * Auth user can see name, price and buy button.
     *
     * @return void
     */
    public function test_auth_user_can_see_products_list()
    {
        $products = $this->get_101_products();
        $user = $this->create_user();

        $response = $this
            ->actingAs($user)
            ->get('/product');
        
        $response->assertOk();
    }

    public function test_admin_can_store_product(): void
    {
        $user = $this->create_admin();
        $response = $this
            ->actingAs($user)
            ->post('/product', [
                'name' => 'Test product',
                'price' => 9.99,
                'tax' => 1.00,
            ]);

        $response->assertRedirect('/product');
    }
}
