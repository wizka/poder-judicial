<?php

namespace App\Http\Controllers;


use App\Models\Purchase;
use Illuminate\Http\Request;

class DashboardController extends Controller
{
    /**
     * Display a listing of purchases by client.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $user = $request->user();
        if ($user->can('view all invoices'))
        {
            $purchases = Purchase::where('invoice_id', null)->paginate();
        } else
        {
            $purchases = Purchase::where('user_id', $user->id)
            ->where('invoice_id', null)
            ->paginate();
        }
       
        return view('dashboard', compact('purchases'));
    }
}
