<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreInvoiceRequest;
use App\Http\Requests\UpdateInvoiceRequest;
use App\Models\Invoice;
use Illuminate\Http\Request;
use App\Models\Purchase;


class InvoiceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $user = $request->user();
        if ($user->can('view all invoices'))
        {
            $invoices = Invoice::latest()->paginate();
        } else
        {
            $invoices = Invoice::where('user_id', $user->id)->paginate();
        }
       
        return view('invoice.index', compact('invoices'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $purchases = Purchase::where('user_id', $request->user_id)
        ->where('invoice_id', null)
        ->get();
        $total_price = $purchases->sum('price_today');
        $total_tax = $purchases->sum('tax_today');
        $total = $total_price + $total_tax;
        $user_id = $request->user_id;
        return view('invoice.create', compact(
            'purchases', 'total_price', 'total_tax', 'total', 'user_id'
        ));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\StoreInvoiceRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreInvoiceRequest $request)
    {
        $purchases = Purchase::where('user_id', $request->user_id)
        ->where('invoice_id', null)
        ->get();
        $total_price = $purchases->sum('price_today');
        $total_tax = $purchases->sum('tax_today');
        $total = $total_price + $total_tax;
        $user_id = $request->user_id;

        $invoice = New Invoice;
        $invoice->user_id = $user_id;
        $invoice->total = $total;
        $invoice->save();

        foreach ($purchases as $purchase) {
            $purchase->invoice_id = $invoice->id;
            $purchase->save();
        }
        return redirect()->route('dashboard');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Invoice  $invoice
     * @return \Illuminate\Http\Response
     */
    public function show(Invoice $invoice)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Invoice  $invoice
     * @return \Illuminate\Http\Response
     */
    public function edit(Invoice $invoice)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\UpdateInvoiceRequest  $request
     * @param  \App\Models\Invoice  $invoice
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateInvoiceRequest $request, Invoice $invoice)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Invoice  $invoice
     * @return \Illuminate\Http\Response
     */
    public function destroy(Invoice $invoice)
    {
        //
    }
}
