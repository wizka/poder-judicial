<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StorePurchaseRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {
        return ['product_id'    => ['required', 'exists:App\Models\Product,id'],
                'price_today'     => ['required','numeric', 'min:1','max:999.99', 'regex:/^\d+(\.\d{1,2})?$/'],
                'tax_today'       => ['required','numeric', 'min:1','max:99.99', 'regex:/^\d+(\.\d{1,2})?$/']];
    }
}
